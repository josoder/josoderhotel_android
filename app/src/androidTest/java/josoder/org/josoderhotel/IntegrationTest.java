package josoder.org.josoderhotel;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.action.Swiper;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;;
import static org.junit.Assert.*;

/**
 *  Created by Joakim Söderstrand
 *  Integration test of the application
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class IntegrationTest {
    // This is the test user stored in the db.
    private static final String USERNAME = "josoder";
    private static final String PASSWORD = "hemligt";

    /**
     * Start in the MainActivity
     */
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<MainActivity>(MainActivity.class);


    /**
     * Should not display bookings for non authorized users.
     */
    @Test
    public void testNotAuthorized(){
        assertFalse(clickBooking());
    }

    /**
     * Test the login function.
     * The bookings item should now be clickable.
     */
    @Test
    public void testLogin(){
        login();
        assertTrue(clickBooking());
    }

    /**
     * Logout user.
     */
    @Test
    public void testLogout(){
        login();
        logout();
        isOnTheHomePage();
        assertFalse(clickBooking());
    }

    /**
     * Test the navigation drawers home item.
     * @return
     */
    @Test
    public void pressHome(){
        // verify that the correct fragment is loaded
        clickNavDrawerItem("Home");
        onView(withId(R.id.home_welcome))
                .check(matches(withText("Josoder Hotel")));
    }

    /**
     * Test the navigation drawers Nearby item.
     * @return
     */
    @Test
    public void pressNearby(){
        // verify that the correct fragment is loaded
        clickNavDrawerItem("Nearby");
        onView(withId(R.id.eat_btn))
                .check(matches(withText("eat")));
    }

    /**
     * Test the navigation drawers Forecast item.
     * @return
     */
    @Test
    public void pressForecast(){
        // verify that the correct fragment is loaded
        clickNavDrawerItem("Forecast");
        onView(withId(R.id.forecast_description))
                .check(matches(withText("10 day forecast for Oslo")));
    }

    /**
     * Will fail if we are not on the HomeFragment
     */
    private void isOnTheHomePage(){
        onView(withId(R.id.home_welcome))
                .check(matches(withText("Josoder Hotel")));
    }

    /**
     * Click on the item containing the given text from the parameter.
     * @param text
     */
    private void clickNavDrawerItem(String text){
        onView(withId(R.id.drawer_layout))
                .perform(DrawerActions.open());
        onView(withText(text))
                .perform(click());
    }

    /**
     * Try to click the booking item(user need to be logged in for the item to be visible).
     * @return
     */
    private boolean clickBooking(){
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        try {
            onView(withText("Bookings"))
                    .perform(click());
        }
        catch (NoMatchingViewException e){
            Log.e("clickBooking", e.toString());
            return false;
        }
        return true;
    }

    /**
     * Logout
     */
    private void logout(){
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText("Log out"))
                .perform(click());
    }

    /**
     * Log in with the test users credentials
     */
    private void login(){
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText("Log in"))
                .perform(click());
        onView(withId(R.id.username_et))
                .perform(clearText(), typeText(USERNAME));
        onView(withId(R.id.password_et))
                .perform(clearText(), typeText(PASSWORD));
        onView(withId(R.id.ok_btn))
                .perform(click());
    }
}
