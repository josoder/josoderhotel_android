package josoder.org.josoderhotel;

import android.support.v4.app.FragmentManager;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import josoder.org.josoderhotel.pojo.SessionHelper;

/**
 * Created by Joakim Söderstrand
 * The MainActivity serves as a container for the entire application.
 * It contains a NavigationDrawer and options menu.
 * When the user clicks on either of the menus the corresponding fragment is loaded into the framelayout.
 */
public class MainActivity extends AppCompatActivity implements LoginFragment.OnLoginFragmentListener {
    private String[] mMenuStringValues;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private ActionBarDrawerToggle mDrawerToggle;
    private SessionHelper mSessionHelper;
    private String mToolbarTitle;

    // user menu items
    private MenuItem[] mUserMenuItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSessionHelper = SessionHelper.getInstance(getBaseContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(HomeFragment.newInstance(null));
        initComponents();
    }

    /**
     * Initialize the components.
     */
    private void initComponents() {
        mMenuStringValues = getResources().getStringArray(R.array.menu_values);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerListView = (ListView) findViewById(R.id.left_drawer);

        mDrawerListView.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mMenuStringValues));
        mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());

        mToolbarTitle = "Home";

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.open_drawer,  /* "open drawer" description for accessibility */
                R.string.close_drawer /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mToolbarTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("Menu");
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        // Store the items in an array for convenience when handling onClick later
        mUserMenuItems = new MenuItem[4];
        for (int i = 0; i < 4; i++) {
            mUserMenuItems[i] = menu.getItem(i);
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, when the options menu is pressed, close it.
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerListView);
        if (drawerOpen) mDrawerLayout.closeDrawer(mDrawerListView);

        return super.onPrepareOptionsMenu(menu);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItem(i);
        }
    }

    /**
     * Handle when user presses an item on the options menu.(Replace the current fragment or logout)
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Open navigation drawer(handles button click on the bar items)
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle the user menu(options menu)
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.login_item) {
            fragment = LoginFragment.newInstance(null);
            mToolbarTitle = getResources().getString(R.string.login);
            loadFragment(fragment);
            mToolbarTitle="Log in";
        }
        else if(id==R.id.logout_item){
            mSessionHelper.logout();
            updateUserMenu();
            fragment = HomeFragment.newInstance(null);
            loadFragment(fragment);
            mToolbarTitle="Home";
        }
        else if(id==R.id.bookings_item){
            fragment = BookingFragment.newInstance(null);
            mToolbarTitle="My Bookings";
            loadFragment(fragment);
        }
        if(fragment!=null) getSupportActionBar().setTitle(mToolbarTitle);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Handles click on the navigation items.
     * Similar to the options menu's click handler. See above..
     * @param position
     */
    private void selectItem(int position) {
        Fragment fragment=null;

        // 0 = home, 1 = nearby, 2 = forecast
        if (position == 0) {
            fragment = HomeFragment.newInstance(null);
        }
        if (position == 1) {
            fragment = MapViewFragment.newInstance(MapViewFragment.BUTTONS_VISIBLE);
        } else if (position == 2) {
            fragment = WeatherFragment.newInstance(null);
        }
        if(fragment!=null) loadFragment(fragment);

        // update selected item and title, then close the drawer
        mDrawerListView.setItemChecked(position, true);
        mToolbarTitle = mMenuStringValues[position];
        mDrawerLayout.closeDrawer(mDrawerListView);
    }

    /**
     * Replaces the current fragment with the one given in the parameter.
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    /**
     * If user is not logged in only display the login item, otherwise display all the other
     * items.
     * (The default seen in the xml is reversed.)
     */
    private void updateUserMenu() {
        for (int i = 0; i < mUserMenuItems.length; i++) {
            mUserMenuItems[i].setVisible(!mUserMenuItems[i].isVisible());
        }
    }


    /**
     * Close the menu when back is pressed.(if it's open)
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerListView)) {
            mDrawerLayout.closeDrawer(mDrawerListView);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Handle callbacks from the LoginFragment
     * @param loggedIn
     */
    @Override
    public void onLoginInteraction(boolean loggedIn) {
        if (loggedIn) {
            updateUserMenu();
            loadFragment(UserDetailsFragment.newInstance(null));
        }
    }

    // Needed for the nav drawer to sync appropriately

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}
