package josoder.org.josoderhotel.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import josoder.org.josoderhotel.pojo.Booking;
import josoder.org.josoderhotel.pojo.User;

/**
 * Created by Joakim Söderstrand
 * DB helper class, creates and provides methods for queries.
 * Inspired by the sql chapter from the book big nerd ranch Android Programming guide 3rd edition.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";
    private static final int VERSION = 1;
    private Context context;

    // DB properties
    private static final String DB_NAME = "josoder.sql";
    // User table
    private static final String TABLE_USER = "user";
    private static final String USER_COLUMN_ID = "_id";
    private static final String USER_COLUMN_NAME = "name";
    private static final String USER_COLUMN_USERNAME = "username";
    private static final String USER_COLUMN_PASSWORD = "password";

    // Booking table
    private static final String TABLE_BOOKING = "booking";
    private static final String BOOKING_COLUMN_FROM_DATE = "from_date";
    private static final String BOOKING_COLUMN_TO_DATE = "to_date";
    private static final String BOOKING_COLUMN_ROOM_TYPE = "type";
    private static final String BOOKING_COLUMN_USER_ID = "user_id";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    /**
     * Create the tables
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE user (" +
                "_id integer primary key autoincrement," +
                "name varchar(100)," +
                "username varchar(50) unique not null," +
                "password varchar(50))");

        db.execSQL("CREATE TABLE booking (" +
                "from_date varchar(50)," +
                "to_date varchar(50), "+
                "type integer," +
                "user_id integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    /**
     * Insert a booking to the DB.
     * @param booking
     * @return long -> the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long insertBooking(Booking booking){
        ContentValues cv = new ContentValues();
        cv.put(BOOKING_COLUMN_USER_ID, booking.getUserId());
        cv.put(BOOKING_COLUMN_FROM_DATE, booking.getFromDate().getTime());
        cv.put(BOOKING_COLUMN_TO_DATE, booking.getToDate().getTime());
        cv.put(BOOKING_COLUMN_ROOM_TYPE, booking.getRoomType());
        return getWritableDatabase().insert(TABLE_BOOKING, null, cv);
    }


    /**
     *  Insert a user to the DB
     * @param user
     * @return long -> the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long insertUser(User user){
        ContentValues cv = new ContentValues();
        cv.put(USER_COLUMN_ID, user.getId());
        cv.put(USER_COLUMN_NAME, user.getName());
        cv.put(USER_COLUMN_USERNAME, user.getUsername());
        cv.put(USER_COLUMN_PASSWORD, user.getPassword());
        return getWritableDatabase().insert(TABLE_USER, null, cv);
    }

    /**
     * Get all bookings for the user associated with the username
     * @param username
     * @return ArrayList
     */
    public List<Booking> getBookings(String username){
        User u =  getUserWithUsername(username);
        if(u==null) return null;

        Cursor c = getReadableDatabase().query(TABLE_BOOKING,
                null, // select all columns
                BOOKING_COLUMN_USER_ID + "= ?", // where username =
                new String[]{String.valueOf(u.getId())}, // user sent with param (injection safe)
                null,  // group by
                null, // having
                null); // all rows)

        BookingCursor bookingCursor = new BookingCursor(c);
        ArrayList<Booking> bookings = new ArrayList<>();
        try {
            if(bookingCursor.getCount()==0) return bookings;
            bookingCursor.moveToFirst();
            while (!bookingCursor.isAfterLast()) {
                bookings.add(bookingCursor.getBooking());
                bookingCursor.moveToNext();
            }
        } finally {
            bookingCursor.close();
        }

        return bookings;
    }

    /**
     * Queries for the user with the given username and returns it.
     * @param username
     * @return user
     */
    public User getUserWithUsername(String username){
        Cursor wrapped = getReadableDatabase().query(TABLE_USER,
                null, // select all columns
                USER_COLUMN_USERNAME + "= ?", // where username =
                new String[]{username}, // user sent with param (injection safe)
                null,  // group by
                null, // having
                "1"); // only one row
        UserCursor userCursor = new UserCursor(wrapped);

        try{
            // no matches
            if(userCursor.getCount()==0){
                return null;
            }
            userCursor.moveToFirst();
            return userCursor.getUser();
        } finally {
            userCursor.close();
        }
    }

    /**
     * Get all users in the db
     * @return UserCursor
     */
    public UserCursor queryAllUsers(){
        Cursor userWrapped = getReadableDatabase().query(TABLE_USER,
                null, null, null, null, null, null);
        return new UserCursor(userWrapped);
    }

    /**
     * Tailored cursor for queries performed on the user table.
     * Allows the ability to get a User object directly from the cursor.
     */
    public static class UserCursor extends CursorWrapper {

        public UserCursor(Cursor c) {
            super(c);
        }


        public User getUser() {
            if (isBeforeFirst() || isAfterLast())
                return null;
            User user = new User();
            user.setId(getLong(getColumnIndex(USER_COLUMN_ID)));
            user.setName(getString(getColumnIndex(USER_COLUMN_NAME)));
            user.setUsername(getString(getColumnIndex(USER_COLUMN_USERNAME)));
            user.setPassword(getString(getColumnIndex(USER_COLUMN_PASSWORD)));
            return user;
        }
    }

    /**
     * Tailored cursor for queries performed on the booking table.
     * Similar to the class implemented above.
     */
    public static class BookingCursor extends CursorWrapper {

        public BookingCursor(Cursor c) {
            super(c);
        }


        public Booking getBooking() {
            if (isBeforeFirst() || isAfterLast())
                return null;
            Booking booking = new Booking();
            booking.setUserId(getLong(getColumnIndex(BOOKING_COLUMN_USER_ID)));
            booking.setFromDate(new Date(getLong(getColumnIndex(BOOKING_COLUMN_FROM_DATE))));
            booking.setToDate(new Date(getLong(getColumnIndex(BOOKING_COLUMN_TO_DATE))));
            booking.setRoomType(getInt(getColumnIndex(BOOKING_COLUMN_ROOM_TYPE)));
            return booking;
        }
    }


}
