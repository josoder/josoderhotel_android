package josoder.org.josoderhotel;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import josoder.org.josoderhotel.adapter.BookingRecyclerViewAdapter;
import josoder.org.josoderhotel.pojo.Booking;
import josoder.org.josoderhotel.pojo.SessionHelper;


/**
 * Created by Joakim Söderstrand
 * A fragment representing a list of bookings.
 * interface.
 */
public class BookingFragment extends Fragment {
    private static final String STRING_ARG = "param";
    private String mParam;
    private BookingRecyclerViewAdapter mAdapter;
    private List<Booking> mBookings;
    private SessionHelper mSession;

    public BookingFragment() {
    }

    /**
     * Factory method
     * @param param
     * @return
     */
    public static BookingFragment newInstance(String param) {
        BookingFragment fragment = new BookingFragment();
        Bundle args = new Bundle();
        args.putString(STRING_ARG, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam = getArguments().getString(STRING_ARG);
        }
        mSession = SessionHelper.getInstance(getContext());
        mBookings = mSession.getBookings();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mAdapter = new BookingRecyclerViewAdapter(mBookings);
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        }
        return view;
    }

}
