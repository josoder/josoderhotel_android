package josoder.org.josoderhotel.pojo;

/**
 * Created by Joakim Söderstrand
 * WeatherItem representing the information parsed in the WeatherFetcher
 */
public class WeatherItem {
    private int weatherSymbol;
    private int precipitation ;
    private double temperature;
    private String date;

    public WeatherItem() {
    }

    public WeatherItem(int weatherSymbol, int precipitation, double temperature, String date) {
        this.weatherSymbol = weatherSymbol;
        this.precipitation = precipitation;
        this.temperature = temperature;
        this.date = date;
    }

    public int getWeatherSymbol() {
        return weatherSymbol;
    }

    public void setWeatherSymbol(int weatherSymbol) {
        this.weatherSymbol = weatherSymbol;
    }

    public int getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(int precipitation) {
        this.precipitation = precipitation;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
