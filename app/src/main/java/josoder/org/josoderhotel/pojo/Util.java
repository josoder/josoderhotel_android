package josoder.org.josoderhotel.pojo;

import android.app.Activity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Joakim Söderstrand
 * Utility class with convenient methods.
 */

public class Util {
    private static final String TAG = "Util";

    /**
     * Hide the keyboard in the given activity.
     * see http://stackoverflow.com/questions/4165414/how-to-hide-soft-keyboard-on-android-after-clicking-outside-edittext
     * @param activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    /**
     * Convert UTC to local time.
     * @param UTC
     * @return local
     */
    public static String convertFromUTCToCET(String UTC){
        String local;

        try {
            SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date utcDate = utcFormat.parse(UTC);

            SimpleDateFormat localDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            localDate.setTimeZone(TimeZone.getDefault());
            local = localDate.format(utcDate);
        }
        catch (Exception e){
            Log.e(TAG, e.toString());
            return null;
        }

        return local;
    }
}
