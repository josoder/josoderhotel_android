package josoder.org.josoderhotel.pojo;

import java.util.Date;

/**
 * Created by Joakim Södertrand
 * Objects representing a booking
 */
public class Booking {
    public static String[] ROOM_TYPE={"", "single", "double", "penthouse"};
    private long id;
    private Date fromDate;
    private Date toDate;
    private int roomType;
    private long userId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }
}
