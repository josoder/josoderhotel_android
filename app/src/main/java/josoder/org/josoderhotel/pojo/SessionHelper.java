package josoder.org.josoderhotel.pojo;

import android.content.Context;
import android.database.SQLException;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import josoder.org.josoderhotel.database.DatabaseHelper;

/**
 * Created by Joakim Söderstrand
 * Singleton class used to handle session details.
 * Provides functionality for authentication and serves as a facade to the sqlite-provider.
 * Note: This implementation is only for display purposes and is not secure enough, there is
 * no hashing etc..
 */
public class SessionHelper {
    private final String TAG = "SessionHelper";
    private static SessionHelper session;
    private DatabaseHelper db;
    private User loggedInUser;

    /**
     * Get instance of SessionHelper.
     * @param context
     * @return
     */
    public static SessionHelper getInstance(Context context){
        if(session==null){
            session = new SessionHelper(context);
            session.addMockedUsers();
        }

        return session;
    }

    private SessionHelper(Context context){
        db = new DatabaseHelper(context);
        loggedInUser = null;
    }

    /**
     * Authenticate user.
     * Return true if credentials are valid.
     * @param username
     * @param password
     * @return
     */
    public boolean login(String username, String password){
        if(TextUtils.isEmpty(username)||TextUtils.isEmpty(password)){
            return false;
        }

        User u = db.getUserWithUsername(username);
        if(u==null||!u.getPassword().equals(password)){
            return false;
        }

        loggedInUser = u;

        return true;

    }

    public void logout(){
        loggedInUser = null;
    }

    public User loggedInUser(){
        return loggedInUser;
    }


    /**
     * Get bookings for the current user.
     * @return List
     */
    public List<Booking> getBookings(){
        this.loggedInUser = db.getUserWithUsername("josoder");
        Log.i(TAG, "" + db.getBookings(loggedInUser.getUsername()));

        if(loggedInUser==null) return new ArrayList<Booking>();
        else
        return db.getBookings(loggedInUser.getUsername());
    }

    /**
     * Adds a test user, to the user table
     */
    private void addMockedUsers(){
        User testUser = new User();
        if (db.getUserWithUsername("josoder")!=null) return;
        testUser.setName("Joakim Söderstrand");
        testUser.setPassword("hemligt");
        testUser.setUsername("josoder");
        try {
            db.insertUser(testUser);

        }
        catch (SQLException e){
            Log.e(TAG, e.toString());
        }

        addMockedBookings("josoder");
    }

    /**
     * Adds test data, a booking to the booking table.
     * @param username
     */
    private void addMockedBookings(String username){
        Booking booking = new Booking();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = "2017-07-20";
        String date2 = "2017-07-23";
        try {
            booking.setFromDate(df.parse(date1));
            booking.setToDate(df.parse(date2));
            booking.setUserId(db.getUserWithUsername(username).getId());
            booking.setRoomType(1);
            Log.i(TAG, "" + db.insertBooking(booking));
        }
        catch (Exception e){
            Log.e(TAG, e.toString());
        }
    }
}
