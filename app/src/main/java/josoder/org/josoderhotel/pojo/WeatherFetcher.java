package josoder.org.josoderhotel.pojo;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joakim Söderstrand
 * This class is specially implemented for smhi's opendata. See http://opendata.smhi.se/apidocs/.
 * It makes a request based on the location set in OSLO_LAT, OSLO_LON, to get a 10 day weather forecast.
 * The json response is than parsed and returned as a list.
 * The structure of the class is inspired by the The Big Nerd Ranch Android Programming Guide 2nd edition.
 */
public class WeatherFetcher {
    private static final String TAG = "WeatherFetcher";
    private static String OSLO_LAT="59.913772";
    private static String OSLO_LON="10.751579";

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }
    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public List<WeatherItem> fetchItems() {
        List<WeatherItem> items = new ArrayList<>();
        try {
            String url = Uri.parse("https://opendata-download-metfcst.smhi.se/api/category/pmp2g/version/2/geotype/point/lon/" + OSLO_LON+"/lat/" + OSLO_LAT + "/data.json").toString();
            String jsonString = getUrlString(url);
            Log.i(TAG, "Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseItems(items, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return items;
    }

    /**
     * This parser is specifically designed for SMHIs open data API - Meteorological Forecasts
     * The structure of the object can be seen at:  http://opendata.smhi.se/apidocs/metfcst/examples.html
     * @param items
     * @param jsonBody
     * @throws IOException
     * @throws JSONException
     */
    private void parseItems(List<WeatherItem> items, JSONObject jsonBody)
            throws IOException, JSONException {

        // TimeSeries contains a list of weather information given in a sequence
        // during the day
        JSONArray weatherJsonArray = jsonBody.getJSONArray("timeSeries");
        Log.i(TAG, " " + weatherJsonArray.length());


        // the values we are interested of:
        double temp=0; // air temp
        int wSymb=0; // weather symbol
        int pcat=0; // precipitation category


        for (int i = 0; i < weatherJsonArray.length(); i++) {
            String date = Util.convertFromUTCToCET(weatherJsonArray.getJSONObject(i).getString("validTime"));
            // Every forecast for any given time contains a set of parameters
            JSONArray weatherJsonParameters = weatherJsonArray.getJSONObject(i).getJSONArray("parameters");
            for(int j=0; j<weatherJsonParameters.length(); j++){
                JSONObject param = weatherJsonParameters.getJSONObject(j);


                // set temperature
                if(param.getString("name").equals("t")){
                    JSONArray vals = param.getJSONArray("values");
                    temp = vals.getDouble(0);
                }

                // set weather symbol
                if(param.getString("name").equals("Wsymb")){
                    JSONArray vals = param.getJSONArray("values");
                    wSymb = vals.getInt(0);
                }

                // set precipitation category
                if(param.getString("name").equals("pcat")){
                    JSONArray vals = param.getJSONArray("values");
                    pcat = vals.getInt(0);
                }
            }

            //Log.i(TAG, "" + temp + "," + wSymb + "," + pcat + "," + date);

            items.add(new WeatherItem(wSymb, pcat, temp, date));
        }



    }



}
