package josoder.org.josoderhotel.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import josoder.org.josoderhotel.R;
import josoder.org.josoderhotel.pojo.Booking;

import java.util.List;

/**
 *  Created by Joakim Söderstrand
 *  Adapter and viewholder for the booking recyclerview.
 */
public class BookingRecyclerViewAdapter extends RecyclerView.Adapter<BookingRecyclerViewAdapter.ViewHolder> {

    private final List<Booking> mValues;

    public BookingRecyclerViewAdapter(List<Booking> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_booking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mRoomTypeTv.setText("Type of room: " + Booking.ROOM_TYPE[mValues.get(position).getRoomType()]);
        holder.mToDateTv.setText("To: " + mValues.get(position).getToDate().toString().substring(0, 10));
        holder.mFromDateTv.setText("From: " + mValues.get(position).getFromDate().toString().substring(0, 10));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView mFromDateTv;
        public TextView mToDateTv;
        public TextView mRoomTypeTv;
        public Booking mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mFromDateTv = (TextView) view.findViewById(R.id.from_date_tv);
            mToDateTv = (TextView) view.findViewById(R.id.to_date_tv);
            mRoomTypeTv = (TextView) view.findViewById(R.id.booking_room_type_tv);
        }


    }
}
