package josoder.org.josoderhotel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import josoder.org.josoderhotel.R;
import josoder.org.josoderhotel.pojo.WeatherItem;

import java.util.List;

/**
 *  Created by Joakim Söderstrand
 *  Adapter and viewholder for the weather recyclerview
 */
public class WeatherItemRecyclerViewAdapter extends RecyclerView.Adapter<WeatherItemRecyclerViewAdapter.ViewHolder> {
    private List<WeatherItem> mValues;
    private Context mContext;

    public WeatherItemRecyclerViewAdapter(List<WeatherItem> items, Context context) {
        mValues = items;
        mContext = context;
    }

    public List<WeatherItem> getmValues() {
        return mValues;
    }

    public void setmValues(List<WeatherItem> mValues) {
        this.mValues = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_weatheritem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mDateTv.setText(mValues.get(position).getDate());
        String weatherDescription = mContext.getResources().getStringArray(R.array.weather_symbol)[mValues.get(position).getWeatherSymbol()];
        double temp = mValues.get(position).getTemperature();
        holder.mDescrTv.setText("" + temp + "°C " + " with " + weatherDescription.toLowerCase());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDateTv;
        public final TextView mDescrTv;
        public WeatherItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDateTv = (TextView) view.findViewById(R.id.date_tv);
            mDescrTv = (TextView) view.findViewById(R.id.description_tv);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDescrTv.getText() + "'";
        }
    }
}
