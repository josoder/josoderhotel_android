package josoder.org.josoderhotel;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import josoder.org.josoderhotel.adapter.WeatherItemRecyclerViewAdapter;
import josoder.org.josoderhotel.pojo.WeatherFetcher;
import josoder.org.josoderhotel.pojo.WeatherItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joakim Söderstrand
 * A fragment representing a list of Items.
 */
public class WeatherFragment extends Fragment {
    private static String ARG_PARAM = "param";
    private String mParam;
    private List<WeatherItem> mForecast;
    private WeatherItemRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WeatherFragment() {
    }

    /**
     * Create a new instance of this fragment, with the parameter specified.
     *
     * @param param
     * @return fragment
     */
    public static WeatherFragment newInstance(String param) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mForecast = new ArrayList<>(); // This is to avoid nullpointer exception in case the background task is not completed
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weatheritem_list, container, false);
        new FetchItemsTask().execute();

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new WeatherItemRecyclerViewAdapter(mForecast, getContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        return view;
    }

    private void updateUI() {
        mAdapter.setmValues(mForecast);
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Asynchronously fetches the items
     */
    private class FetchItemsTask extends AsyncTask<Void, Void, Void> {
        /**
         * This is needed to avoid nullpointer exception
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mForecast = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... params) {
            mForecast = new WeatherFetcher().fetchItems();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateUI();
        }
    }
}
