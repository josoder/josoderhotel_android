package josoder.org.josoderhotel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

;


/**
 * Created by Joakim Söderstrand
 * MapViewFragment holds a map view.
 * A Google map object is used to manipulate the map.
 * This fragments holds 3 static locations which the user can choose between.
 * When the corresponding button is pressed the camera will move to the location with an animation.
 * If the visible parameter is not set to BUTTONS_VISIBLE the buttons wont be visible which is convenient when using
 * this fragment as a nested frame. See @MainActivity and @Home for an example of the 2 different
 * use cases.
 */
public class MapViewFragment extends Fragment implements OnMapReadyCallback,
        View.OnClickListener{
    private static final LatLng EAT =  new LatLng(59.9341452,10.7626625);
    private static final LatLng DRINK = new LatLng(59.9361493,10.7655723);
    private static final LatLng MOVIES = new LatLng(59.9286306,10.7607095);
    private static final String ARG_VISIBILITY = "param";
    public static int BUTTONS_VISIBLE = 1;
    private int mVisibility;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private MarkerOptions mHotel, mEat, mMovies, mDrink;
    private Button mEatBtn, mDrinkBtn, mMoviesBtn;

    public MapViewFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * If the param is set to 1, the buttons will be visible.
     * See class documentation for more info.
     * @param param
     * @return A new instance of fragment MapViewFragment.
     */
    public static MapViewFragment newInstance(int buttonVisibility) {
        MapViewFragment fragment = new MapViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_VISIBILITY, buttonVisibility);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mVisibility = getArguments().getInt(ARG_VISIBILITY);
        }
        setUpMarkers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_view, container, false);
        if(mVisibility == BUTTONS_VISIBLE){
            LinearLayout buttonLayout = (LinearLayout) view.findViewById(R.id.button_layout);
            buttonLayout.setVisibility(View.VISIBLE);
            mMoviesBtn = (Button) view.findViewById(R.id.movies_btn);
            mMoviesBtn.setOnClickListener(this);
            mEatBtn = (Button) view.findViewById(R.id.eat_btn);
            mEatBtn.setOnClickListener(this);
            mDrinkBtn = (Button) view.findViewById(R.id.drink_btn);
            mDrinkBtn.setOnClickListener(this);
        }
        return view;
    }

    /**
     * When the view is created the map is initialized.
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        // Start to load the map asynchronously, notify when ready
        mMapView.getMapAsync(this);
    }

    /**
     * When map is ready, set the static locations.
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap  = googleMap;
        LatLng oslo = new LatLng(59.9335957,10.7656221);
        CameraPosition target = CameraPosition.builder()
                .target(oslo)
                .zoom(17)
                .build();
        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(target));
        mGoogleMap.addMarker(mHotel);
        mGoogleMap.addMarker(mEat).setAlpha(0.5f);
        mGoogleMap.addMarker(mDrink).setAlpha(0.5f);
        mGoogleMap.addMarker(mMovies).setAlpha(0.5f);
    }

    /**
     * Move the camera with an animation effect.
     * @param position
     */
    public void moveCamera(LatLng position){
        CameraPosition target = CameraPosition.builder()
                .target(position)
                .zoom(17)
                .build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(target), 2000, null);
    }

    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public void onClick(View v) {
        if(v==mMoviesBtn){
            moveCamera(MOVIES);
        }
        else if(v==mDrinkBtn){
            moveCamera(DRINK);
        }
        else if(v==mEatBtn){
            moveCamera(EAT);
        }
    }

    /**
     * Initialize the markers.
     */
    private void setUpMarkers(){
        mHotel = new MarkerOptions()
                .position(new LatLng(59.9335957,10.7656221))
                .title("josoderhotel");

        mEat = new MarkerOptions()
                .position(DRINK);

        mDrink = new MarkerOptions()
                .position(EAT);

        mMovies = new MarkerOptions()
                .position(MOVIES);
    }
}
