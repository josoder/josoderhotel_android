package josoder.org.josoderhotel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import josoder.org.josoderhotel.pojo.SessionHelper;
import josoder.org.josoderhotel.pojo.User;


/**
 * Created by Joakim Söderstrand
 * Fragment displays user details
 */
public class UserDetailsFragment extends Fragment {
    private static final String ARG_PARAM = "param";
    private SessionHelper mSessionHelper;
    private User mUser;
    private TextView mWelcomeTv;
    private String mParam;


    public UserDetailsFragment() {}

    /**
     * Factory method
     * @param param
     * @return fragment (new instance of UserDetailFragment)
     */
    public static UserDetailsFragment newInstance(String param) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
        }
        mSessionHelper = SessionHelper.getInstance(getContext());
        mUser = mSessionHelper.loggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        mWelcomeTv =  (TextView)view.findViewById(R.id.welcome_tv);
        mWelcomeTv.setText("Logged in as " +  mUser.getName());
        return view;
    }

}
