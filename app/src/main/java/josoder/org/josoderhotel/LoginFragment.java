package josoder.org.josoderhotel;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import josoder.org.josoderhotel.pojo.SessionHelper;
import josoder.org.josoderhotel.pojo.Util;


/**
 * Created by Joakim Söderstrand
 * Implements a fragment where the users can login.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM = "param";
    private SessionHelper mSession;
    private EditText mUserNameEt;
    private EditText mPasswordEt;
    private Button mOkBtn;
    private Button mCancelBtn;

    private String mParam;

    private OnLoginFragmentListener mListener;

    public LoginFragment() {
    }


    public static LoginFragment newInstance(String param) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSession = SessionHelper.getInstance(getContext());
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUserNameEt = (EditText) view.findViewById(R.id.username_et);
        mUserNameEt.setOnClickListener(this);
        mPasswordEt = (EditText) view.findViewById(R.id.password_et);
        mPasswordEt.setOnClickListener(this);
        mCancelBtn = (Button) view.findViewById(R.id.cancel_btn);
        mCancelBtn.setOnClickListener(this);
        mOkBtn = (Button) view.findViewById(R.id.ok_btn);
        mOkBtn.setOnClickListener(this);
        return view;
    }

    /**
     * Notify hosting activity when user has logged in.
     * @param loggedIn
     */
    public void userValidated(boolean loggedIn) {
        if (mListener != null) {
            mListener.onLoginInteraction(loggedIn);
        }
    }

    /**
     * Make sure the interface is implemented in the hosting activity.
     *
     * (Android studio template implementation)
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentListener) {
            mListener = (OnLoginFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Validate user input and try to authenticate.
     * @param v
     */
    @Override
    public void onClick(View v) {
        if(v==mOkBtn){
            Util.hideSoftKeyboard(getActivity());
            if(TextUtils.isEmpty(mUserNameEt.getText().toString())|| TextUtils.isEmpty(mPasswordEt.getText().toString())){
                Toast.makeText(getContext(), "username and/or password field is empty", Toast.LENGTH_LONG).show();
            }
            else if(mSession.login(mUserNameEt.getText().toString(), mPasswordEt.getText().toString())){
                userValidated(true);
            }
            else{
                Toast.makeText(getContext(), "invalid username and/or password ", Toast.LENGTH_LONG).show();
                mPasswordEt.setText("");
            }
        }
        else if(v==mCancelBtn){
            // needed for the keyboard to close
            Util.hideSoftKeyboard(getActivity());
        }
    }


    /**
     * Interface needs to be implemented in the hosting activity.
     * If a user has logged in the hosting activity will be notified.
     */
    public interface OnLoginFragmentListener {
        void onLoginInteraction(boolean loggedIn);
    }
}
